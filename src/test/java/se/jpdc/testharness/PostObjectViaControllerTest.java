package se.jpdc.testharness;

import static org.assertj.core.api.Assertions.assertThat;
//import static org.junit.Assert.assertFalse;
//import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class PostObjectViaControllerTest {
	
    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;
    
    @Autowired
    private MyService service;
    
    private final String WRONG = "not_the_object";
    
    @Test
    public void postObjectViaController() {
    	
    	final String OBJECT_NAME = "test_object";
    	
    	ObjectMapper mapper = new ObjectMapper();	
    	MyObject object = new MyObject();
		object.setName(OBJECT_NAME);
    	
    	String message = null;
    	
    	try {
			message = mapper.writeValueAsString(object);
		} catch (JsonProcessingException e) {
			System.out.println("Could not write object so Json");
		}
    	
    	HttpHeaders headers = new HttpHeaders();
    	headers.setContentType(MediaType.APPLICATION_JSON);
    	
    	HttpEntity<String> entity = new HttpEntity<String>(message ,headers);
    	
    	MyObject response = this.restTemplate.postForObject("http://localhost:" + port + "/register", 
    			entity, MyObject.class);
    	
    	assertThat(response.getName().equals(OBJECT_NAME));
    }
  
    
    @Test
    public void persistObjectViaController() {
    	
    	final String OBJECT_NAME = "persisted_object"; 
    	
    	ObjectMapper mapper = new ObjectMapper();	
    	MyObject object = new MyObject();
    	object.setName(OBJECT_NAME);
    	
    	String message = null;
    	
    	try {
			message = mapper.writeValueAsString(object);
		} catch (JsonProcessingException e) {
			System.out.println("Could not write object so Json");
		}
    	
    	HttpHeaders headers = new HttpHeaders();
    	headers.setContentType(MediaType.APPLICATION_JSON);
    	
    	HttpEntity<String> entity = new HttpEntity<String>(message ,headers);
    	
    	this.restTemplate.postForObject("http://localhost:" + port + "/register", 
    			entity, MyObject.class);
    	
    	MyObject myObject = service.findByName(OBJECT_NAME);
    	
    	assertThat(myObject.getName().equals(OBJECT_NAME));
    }
    
    @Test
    public void postObjectViaControllerAndFail() {
    	
    	final String OBJECT_NAME = "test_object";
    	
    	ObjectMapper mapper = new ObjectMapper();	
    	MyObject object = new MyObject();
		object.setName(OBJECT_NAME);
    	
    	String message = null;
    	
    	try {
			message = mapper.writeValueAsString(object);
		} catch (JsonProcessingException e) {
			System.out.println("Could not write object so Json");
		}
    	
    	HttpHeaders headers = new HttpHeaders();
    	headers.setContentType(MediaType.APPLICATION_JSON);
    	
    	HttpEntity<String> entity = new HttpEntity<String>(message ,headers);
    	
    	MyObject response = this.restTemplate.postForObject("http://localhost:" + port + "/register", 
    			entity, MyObject.class);
    	
    	assertFalse(response.getName().equals(WRONG));
    }
    
    @Test
    public void persistObjectViaControllerAndFail() {
    	
    	final String OBJECT_NAME = "test_object"; 
    	
    	ObjectMapper mapper = new ObjectMapper();	
    	MyObject object = new MyObject();
    	object.setName(OBJECT_NAME);
    	
    	String message = null;
    	
    	try {
			message = mapper.writeValueAsString(object);
		} catch (JsonProcessingException e) {
			System.out.println("Could not write object so Json");
		}
    	
    	HttpHeaders headers = new HttpHeaders();
    	headers.setContentType(MediaType.APPLICATION_JSON);
    	
    	HttpEntity<String> entity = new HttpEntity<String>(message ,headers);
    	
    	this.restTemplate.postForObject("http://localhost:" + port + "/register", 
    			entity, MyObject.class);
    	
    	MyObject myObject = service.findByName(WRONG);
    	
    	assertNull(myObject);
    }
    
}
