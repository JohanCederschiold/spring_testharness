package se.jpdc.testharness;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;


@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class TestharnessApplicationTests {
	
	@LocalServerPort
	private int port;
	
	@Autowired
	MyService service;
	
	@Autowired
	MyController controller;
	
	@Autowired
	TestRestTemplate template;

	@Test
	void contextLoads() {
	}
	
	@Test
	void testStoringSingleUserViaServiceLayer() {
		String storedUser = "Herman Hedning";
		MyObject object = new MyObject();
		object.setName(storedUser);
		service.saveObject(object);
		MyObject retrieved = service.findByName(storedUser);
		assertEquals(storedUser, retrieved.getName(), "Stored user matches expectations");
	}
	
	@Test
	void testStoringSingleUserViaRestService() {
		String storedUser = "Hulken Huligan";
		MyObject object = new MyObject();
		object.setName(storedUser);
		controller.register(object);
		MyObject retrieved = service.findByName(storedUser);
		assertEquals(storedUser, retrieved.getName(), "Stored user matches expectations");
	}
	
	
	
	

}
