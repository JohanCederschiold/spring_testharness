package se.jpdc.testharness;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MyRepo extends JpaRepository<MyObject, Long> {
	
	public MyObject findByName(String name);

}
