package se.jpdc.testharness;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MyController {
	
	@Autowired
	MyService service;
	
	/*
	 *  Av någon anledning kan man inte skicka vidare objektet direkt från controllern. Detta skulle givetvis 
	 *  vara dumt att göra men det hade varit intressant att veta varför det inte går. Därför använder jag ett
	 *  Wrapperobject här. Normalt har jag svarat med en DTO så jag har inte sett det här problemet tidigare.
	 */
	
	
	@PostMapping("/register")
	public MyObject register (@RequestBody MyObject object) {
		return service.saveObject(object);
	}
	
	@GetMapping("/getid/{id}")
	public MyWrapper findId (@PathVariable(value = "id")Long id) {
		System.out.println("I'm in the controller " + id);
		MyObject object = service.retrieveObject(id);
		return new MyWrapper(object.getId(), object.getName());
	}
	
	@GetMapping("/getname/{name}")
	public MyWrapper findName (@PathVariable(value = "name")String name) {
		System.out.println("I'm in the controller " + name);
		MyObject object = service.findByName(name);
		return new MyWrapper(object.getId(), object.getName());
	}
	
	

}
