package se.jpdc.testharness;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MyServiceImpl implements MyService {
	
	@Autowired
	MyRepo repo;

	@Override
	public MyObject saveObject(MyObject mo) {
		
		return repo.save(mo);
	}

	@Override
	public MyObject retrieveObject(Long id) {
		System.out.println("I'm in the service " + id);
		
		return repo.getOne(id);
	}

	@Override
	public MyObject findByName(String name) {
		System.out.println("I'm in the service " + name);
		return repo.findByName(name);
	}

}
