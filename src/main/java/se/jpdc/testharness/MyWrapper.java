package se.jpdc.testharness;

public class MyWrapper {
	
	private Long id;
	private String name;
	
	
	public MyWrapper(Long id, String name) {
		super();
		this.id = id;
		this.name = name;
	}


	public Long getId() {
		return id;
	}


	public String getName() {
		return name;
	}
	
	
	
	

}
