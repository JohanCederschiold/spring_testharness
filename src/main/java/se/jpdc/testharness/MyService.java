package se.jpdc.testharness;

public interface MyService {
	
	MyObject saveObject(MyObject mo);
	MyObject retrieveObject(Long id);
	MyObject findByName(String name);

}
